class ForceClientControls {
  static moduleName = "force-client-controls";
  static forced;
  static unlocked;

  static isGM() {
    return game.user?.can("SETTINGS_MODIFY") ?? false;
  }

  static initialize() {
    game.settings.register(ForceClientControls.moduleName, "forced", {
      name: `${game.i18n.localize("FORCECLIENTCONTROLS.settings.forced.name")}`,
      hint: `${game.i18n.localize("FORCECLIENTCONTROLS.settings.forced.hint")}`,
      scope: "world",
      config: false,
      type: Object,
      default: {},
    });
    game.settings.register(ForceClientControls.moduleName, "unlocked", {
      name: `${game.i18n.localize("FORCECLIENTCONTROLS.settings.unlocked.name")}`,
      hint: `${game.i18n.localize("FORCECLIENTCONTROLS.settings.unlocked.hint")}`,
      scope: "client",
      config: false,
      type: Object,
      default: {},
    });

    ForceClientControls.forced = new Map(Object.entries(game.settings.get(ForceClientControls.moduleName, "forced")));
    ForceClientControls.unlocked = new Map(
      Object.entries(game.settings.get(ForceClientControls.moduleName, "unlocked"))
    );

    libWrapper.register(
      ForceClientControls.moduleName,
      "KeybindingsConfig.defaultOptions",
      ForceClientControls.keybindingsConfigDefaultOptions,
      "WRAPPER"
    );

    libWrapper.register(
      ForceClientControls.moduleName,
      "ClientKeybindings.prototype.initialize",
      ForceClientControls.bindingsInitialize,
      "WRAPPER"
    );
    libWrapper.register(
      ForceClientControls.moduleName,
      "ClientKeybindings.prototype.resetDefaults",
      ForceClientControls.bindingsResetDefaults,
      "WRAPPER"
    );
    libWrapper.register(
      ForceClientControls.moduleName,
      "ClientKeybindings.prototype.get",
      ForceClientControls.bindingsGet,
      "WRAPPER"
    );
    libWrapper.register(
      ForceClientControls.moduleName,
      "ClientKeybindings.prototype.set",
      ForceClientControls.bindingsSet,
      "WRAPPER"
    );
    Hooks.on("renderKeybindingsConfig", ForceClientControls.renderKeybindingsConfig);
  }

  static keybindingsConfigDefaultOptions(wrapped, ...args) {
    return foundry.utils.mergeObject(wrapped(...args), { scrollY: [".category-list", ".scrollable"] });
  }

  static bindingsInitialize(wrapped, ...args) {
    const result = wrapped(...args);
    const forced = ForceClientControls.forced;
    // Register bindings for forced actions
    for (let [action, config] of this.actions) {
      if (
        forced.has(action) &&
        (forced.get(action).mode !== "soft" || ForceClientControls.isGM() || !ForceClientControls.unlocked.has(action))
      ) {
        let bindings = config.uneditable.concat(forced.get(action).bindings ?? config.editable);
        console.log(`Force Client Controls | Forcing ${action}`);
        this.bindings.set(action, bindings);
      }
    }

    // Create a mapping of keys which trigger actions
    this.activeKeys = new Map();
    for (let [key, action] of this.actions) {
      let bindings = this.bindings.get(key);
      for (let binding of bindings) {
        if (!binding) continue;
        if (!this.activeKeys.has(binding.key)) this.activeKeys.set(binding.key, []);
        let actions = this.activeKeys.get(binding.key);
        actions.push({
          action: key,
          key: binding.key,
          name: action.name,
          requiredModifiers: binding.modifiers,
          optionalModifiers: action.reservedModifiers,
          onDown: action.onDown,
          onUp: action.onUp,
          precedence: action.precedence,
          order: action.order,
          repeat: action.repeat,
          restricted: action.restricted,
        });
        this.activeKeys.set(binding.key, actions.sort(this.constructor._compareActions));
      }
    }
    return result;
  }

  static async bindingsSet(wrapped, namespace, action, bindings, ...args) {
    if (!namespace || !action) throw new Error("You must specify both namespace and key portions of the keybind");
    if (!ForceClientControls.isGM()) {
      if (ForceClientControls.forced.has(`${namespace}.${action}`)) {
        const forced = ForceClientControls.forced.get(`${namespace}.${action}`);
        if (forced.mode !== "soft" || !ForceClientControls.unlocked.has(`${namespace}.${action}`)) {
          bindings = forced.bindings;
        }
      }
      return await wrapped(namespace, action, bindings, ...args);
    } else {
      if (ForceClientControls.forced.has(`${namespace}.${action}`)) {
        const forced = ForceClientControls.forced.get(`${namespace}.${action}`);
        forced.bindings = bindings;
        ForceClientControls.forced.set(`${namespace}.${action}`, forced);
        const result = await wrapped(namespace, action, bindings, ...args);
        await game.settings.set(
          ForceClientControls.moduleName,
          "forced",
          Object.fromEntries(ForceClientControls.forced)
        );
        return result;
      } else {
        return await wrapped(namespace, action, bindings, ...args);
      }
    }
  }

  static bindingsGet(wrapped, namespace, action, ...args) {
    let bindings = wrapped(namespace, action, ...args);
    if (ForceClientControls.forced.has(`${namespace}.${action}`)) {
      const forced = ForceClientControls.forced.get(`${namespace}.${action}`);
      if (
        forced.mode !== "soft" ||
        ForceClientControls.isGM() ||
        !ForceClientControls.unlocked.has(`${namespace}.${action}`)
      ) {
        bindings = forced.bindings || [];
      }
    }
    return bindings;
  }

  static async bindingsResetDefaults(wrapped, ...args) {
    ForceClientControls.forced.clear();
    ForceClientControls.unlocked.clear();
    await game.settings.set(ForceClientControls.moduleName, "forced", Object.fromEntries(ForceClientControls.forced));
    await game.settings.set(
      ForceClientControls.moduleName,
      "unlocked",
      Object.fromEntries(ForceClientControls.unlocked)
    );
    return await wrapped(...args);
  }

  static async forceBinding(action, mode) {
    if (!ForceClientControls.isGM()) return;
    ForceClientControls.forced.set(action, {
      mode: mode,
      bindings: game.keybindings.bindings
        .get(action)
        ?.filter((elem) => !game.keybindings.actions.get(action)?.uneditable?.includes(elem)),
    });
    await game.settings.set(ForceClientControls.moduleName, "forced", Object.fromEntries(ForceClientControls.forced));
    game.keybindings.initialize();
  }

  static async unforceBinding(action) {
    if (!ForceClientControls.isGM()) return;
    ForceClientControls.forced.delete(action);
    await game.settings.set(ForceClientControls.moduleName, "forced", Object.fromEntries(ForceClientControls.forced));
    game.keybindings.initialize();
  }

  static async unlockBinding(action) {
    if (ForceClientControls.isGM()) return;
    if (ForceClientControls.forced.has(action)) {
      const forced = ForceClientControls.forced.get(action);
      if (forced.mode === "soft") {
        ForceClientControls.unlocked.set(action, true);
        await game.settings.set(
          ForceClientControls.moduleName,
          "unlocked",
          Object.fromEntries(ForceClientControls.unlocked)
        );
        game.keybindings.initialize();
      }
    }
  }

  static async lockBinding(action) {
    if (ForceClientControls.isGM()) return;
    if (ForceClientControls.forced.has(action)) {
      const forced = ForceClientControls.forced.get(action);
      if (forced.mode === "soft") {
        ForceClientControls.unlocked.delete(action);
        await game.settings.set(
          ForceClientControls.moduleName,
          "unlocked",
          Object.fromEntries(ForceClientControls.unlocked)
        );
        game.keybindings.initialize();
      }
    }
  }

  static async renderKeybindingsConfig(app, html) {
    while (!app.rendered) {
      await new Promise((resolve) => resolve());
    }

    const isGM = ForceClientControls.isGM();
    const fa = {
      "hard-gm": "fa-lock",
      "soft-gm": "fa-unlock",
      "open-gm": "fa-lock-open",
      "hard-client": "fa-lock",
      "soft-client": "fa-unlock",
      "unlocked-client": "fa-lock-open",
    };

    const elem = html.find(".action");
    elem.each(function () {
      const action = $(this).attr("data-action-id");
      let mode = ForceClientControls.forced.get(action)?.mode ?? "open";
      if (mode === "soft" && !isGM && ForceClientControls.unlocked.has(action)) mode = "unlocked";
      mode += isGM ? "-gm" : "-client";
      if (mode !== "open-client") {
        $(this)
          .find("h4, label")
          .first()
          .prepend(
            $("<span>")
              .html("&nbsp;")
              .prop("title", game.i18n.localize(`FORCECLIENTCONTROLS.ui.${mode}-hint`))
              .addClass(`fas ${fa[mode]}`)
              .click(async function (e) {
                await ForceClientControls.clickToggleForceControls(e, action, app);
              })
          );
      }
      if (["hard-client", "soft-client"].includes(mode)) {
        $(this).find("a").css({ "pointer-events": "none", cursor: "default", color: "gray" });
      }
    });
  }

  static async clickToggleForceControls(e, action, app) {
    if (ForceClientControls.isGM()) {
      await app._savePendingEdits();
      let mode = ForceClientControls.forced.get(action)?.mode ?? "open";
      switch (mode) {
        case "open":
          await ForceClientControls.forceBinding(action, "soft");
          break;
        case "soft":
          await ForceClientControls.forceBinding(action, "hard");
          break;
        default:
          await ForceClientControls.unforceBinding(action);
          break;
      }
    } else {
      if (ForceClientControls.forced.get(action)?.mode === "soft") {
        await app._savePendingEdits();
        if (ForceClientControls.unlocked.has(action)) {
          await ForceClientControls.lockBinding(action);
        } else {
          await ForceClientControls.unlockBinding(action);
        }
      }
    }

    app.render();
  }
}

Hooks.once("libWrapper.Ready", ForceClientControls.initialize);
